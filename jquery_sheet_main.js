/*jslint undef: true */
/*global $, document */

// jQuery.sheet.preLoad('jqs/');

// XXX TODO: reallow sheet resizing (CSS issue?)
// XXX TODO: discard changes?


var jquery_sheet_save;

jQuery(document).ready(function ($) {
    "use strict";

    var HiddenColumns = HiddenColumns || {
        'left': [],
        'examples': [],
        'templates': [],
        'list': []
    };

    var csv_parse = function(data) {
        // Using external CSV because the provided parser/tsv.js code is a little buggy
        return CSV.parse(data, {delim:',', quote:'"', rowdelim:'\n'});
    };


    var create_table = function(cells, title) {
        // Return a <table> element filled with data.
        // This is used during initial creation of a spreadsheet.
        var row = 0,
            col = 0,
            $tr = null,
            $table = $('<table>').attr('title', title),
            $colgroup = $('<colgroup>'),
            width = ['15px', '15px', '15px', '15px', '100px', '150px', '100px', '200px'],
            width_length = width.length,
            cells_length = cells.length;

        for (col=0; col<width_length; col+=1) {
            $colgroup.append($('<col>', {width: width[col]}));
        }
        $table.append($colgroup);

        for (row=0; row<cells_length; row+=1) {
            $tr = $('<tr>');
            for (col=0; col<cells[row].length; col+=1) {
                $tr.append($('<td>').text(cells[row][col]));
            }
            $table.append($tr);
        }
        return $table;
    };


    var load_sheets_tables = function($container, tables, options) {
        var i,
            hiddencolumns;
        $container.empty();
        for (i=0; i<tables.length; i+=1) {
            hiddencolumns = (options.hiddenColumns || [])[i] || [];
            if (hiddencolumns) {
                tables[i].attr('data-hiddencolumns', hiddencolumns.join(','));
            }
            $container.append(tables[i]);
        }
        $container.sheet(options);
    };


    var decode_category_dict_to_tables = function(category_dict) {
        var base_category = '',
            tab_title = '',
            table_data = {},
            tab_title_list = [],
            categories = [],
            category = null,
            row_data = null,
            category_path_length = 0,
            i = 0;

        for (base_category in category_dict) {
            if (!category_dict.hasOwnProperty(base_category)) {
                continue;
            }
            tab_title = base_category;
            tab_title_list.push(tab_title);
            categories = category_dict[base_category];
            table_data[tab_title] = [];

            for (i=0; i<categories.length; i+=1) {
                category = categories[i];
                row_data = ['', '', '', ''];
                category_path_length = category.path.split('/').length;
                row_data[category_path_length-2] = '*';
                row_data.push(category.id);
                row_data.push(category.title);
                row_data.push(category.short_title);
                row_data.push(category.description);
                table_data[tab_title].push(row_data);
            }
        }

        return tab_title_list.map(function(tit) {
            if (table_data[tit].length === 0) {
                // at least an empty row must be present, or no header will be displayed
                table_data[tit].push([
                                     '', // A
                                     '', // B
                                     '', // C
                                     '', // D
                                     '', // ID
                                     '', // Title
                                     '', // Short Title
                                     ''  // Description
                                     ]);
            }
            return create_table(table_data[tit], tit);
        });
    };


    var load_sheets_categories = function($container, url, options) {
        $.ajax({
            url: url,
            dataType: 'json'
        }).done(function(data, textStatus, jqXHR) {
            var tables = decode_category_dict_to_tables(data),
                hiddencolumns = [],
                idx = 0;
            for (idx=0; idx<tables.length; idx+=1) {
                hiddencolumns.push(HiddenColumns['left']);
            }
            options['hiddenColumns'] = hiddencolumns;
            load_sheets_tables($container, tables, options);
        }).fail(function(data, textStatus, errorThrown) {
            alert('Could not load categories: ' + errorThrown);
        });
    };

    var update_category_title_saved = function() {
        var $sheet = $('#sheetleft');
        $sheet.find('.jSTitle').text('Categories: Saved');
    };


    var update_category_title_changed = function() {
        var $sheet = $('#sheetleft');
        $sheet.find('.jSTitle').text('Categories: Changed');
    };


    var add_updown_buttons = function() {

        $('<td style="width:50%">')
            .append('<button id="spreadsheet-row-down" title="Move down selected rows" type="button">Down</button>')
            .append('<button id="spreadsheet-row-up" title="Move up selected rows" type="button">Up</button>')
            .append('<button id="spreadsheet-save" title="Save categories" type="button">Save categories</button>')
            .prependTo($('#sheetleft .jSTitle').parent());


        var save_handler = function(event) {
            var $sheet = $('#sheetleft'),
                jS = $sheet.getSheet(),
                sheet_data = $.sheet.dts.fromTables.json(jS);
            $.ajax('sheet_editor_save',
                   {
                       type: 'POST',
                       data: JSON.stringify(sheet_data),
                       contentType: 'application/json'
                   })
                   .done(function(data, textStatus, jqXHR) {
                       update_category_title_saved();
                       if (window.clickSaveButton) {
                         clickSaveButton('Answer_edit');
                       }
                       // $(event.target).submit();
                   })
                   .fail(function(data, textStatus, errorThrown) {
                       alert('Could not save: ' + errorThrown);
                   });
            event.preventDefault();
        };

        $('.save').closest('form').on('submit', save_handler);
        $('#spreadsheet-save').on('click', save_handler);

        var get_selected_rows = function(jS) {
            // Return an array with the indexes of currently selected rows.
            // XXX TODO: Title row should never be selectable.
            var $hlrows = $('#sheetleft tr.ui-state-highlight'),
                idx = 0,
                td = null,
                ret = [];

            for (idx=0; idx<$hlrows.length; idx+=1) {
                td = $hlrows[idx].children[1];
                ret.push(jS.getTdLocation(td).row);
            }
            return ret;
        };

        var swap_rows = function($sheet, row1, row2) {
            // Swap the content of row1 and row2 (values only)
            // therefore metadata (row height, etc) is not swapped.
            var col = 0,
                jS = $sheet.getSheet(),
                rows = jS.rows(),
                num_cols = rows[row1].children.length,
                tmp_value = null;

            for (col=1; col<num_cols; col+=1) {
                tmp_value = $sheet.getCellValue(row1, col, jS.i);
                $sheet.setCellValue($sheet.getCellValue(row2, col, jS.i), row1, col, jS.i);
                $sheet.setCellValue(tmp_value, row2, col, jS.i);
            }
        };

        var number_cmp = function (a, b) {
            return a - b;
        };

        $('#spreadsheet-row-up').click(function(ev) {
            var $sheet = $('#sheetleft'),
                jS = $sheet.getSheet(),
                selected_rows = get_selected_rows(jS),
                idx = 0,
                row_to_move = null;

            selected_rows.sort(number_cmp);

            if (selected_rows[0] > 1) {
                // selected rows cannot move over header row
                for (idx=0; idx<selected_rows.length; idx+=1) {
                    // move up a single row of cells
                    row_to_move = selected_rows[idx];
                    swap_rows($sheet, row_to_move, row_to_move-1);
                }

                // move up row highlight effect
                jS.themeRoller.cell.setHighlighted($('#sheetleft tr.ui-state-highlight').prev());
            }
        });

        $('#spreadsheet-row-down').click(function(ev) {
            var $sheet = $('#sheetleft'),
                jS = $sheet.getSheet(),
                selected_rows = get_selected_rows(jS),
                idx = 0,
                row_to_move = 0;

            selected_rows.sort(number_cmp).reverse();

            if (selected_rows[0] < jS.rows().length-1) {
                // selected rows cannot move after the last row
                for (idx=0; idx<selected_rows.length; idx+=1) {
                    // move down a single row of cells
                    row_to_move = selected_rows[idx];
                    swap_rows($sheet, row_to_move, row_to_move+1);
                }

                // move down row highlight effect
                jS.themeRoller.cell.setHighlighted($('#sheetleft tr.ui-state-highlight').next());
            }
        });

    };



    var decode_suggestion_csv_to_table = function(data, base_category) {
        var rows = csv_parse(data),
            rows = rows.filter(function(row) { return row[0]===base_category; }),
            rows_length = rows.length,
            cells = [],
            i = 0,
            parts = null,
            category_path_length = 0,
            category_id = '',
            category_title = '',
            row_data = null;

        for (i=0; i<rows_length; i+=1) {
            parts = rows[i];
            if (!parts[1]) {
                // skip empty lines
                continue;
            }
            category_path_length = parts[1].split('/').length;
            category_id = parts[2];
            category_title = parts[3];

            row_data = ['', '', '', ''];
            row_data[category_path_length-2] = '*';

            row_data.push(category_id);
            row_data.push(category_title);
            row_data.push('');
            row_data.push('');

            cells.push(row_data);
        }

        return create_table(cells, 'List');
    };



    var decode_example_csv_to_tables = function(data, base_category, tab_title_start) {
        var data_length = data.length,
            idx = 0,
            example = null,
            example_categories = null,
            idx_cat = 0,
            rows = null,
            row_data = null,
            category_path_length = 0,
            category = null,
            tab_title = '',
            ret = [];

        for (idx=0; idx<data_length; idx+=1) {
            example = data[idx];
            example_categories = example.example_categories;
            rows = [];
            for (idx_cat=0; idx_cat<example_categories.length; idx_cat+=1) {
                category = example_categories[idx_cat];
                category_path_length = category['path'].split('/').length;
                row_data = ['', '', '', ''];
                row_data[category_path_length-2] = '*';
                row_data.push(category['id']);
                row_data.push(category['title']);
                row_data.push('');
                row_data.push('');
                if (category.example) {
                    // only for templates
                    row_data.push(category['example']);
                }
                rows.push(row_data);
            }

            tab_title = tab_title_start + ' ' + example.example_id;
            if (example.example_score) {
                tab_title = tab_title + ' (' + example.example_score + ')';
            }

            ret.push(create_table(rows, tab_title));
        }
        return ret;
    };


    var replace_right_sheet = function(base_category, suggestion_data, example_data, template_data) {
        var $sheet = $('#sheetright'),
            jS = $sheet.getSheet(),
            tables = [
                decode_suggestion_csv_to_table(suggestion_data, base_category)
            ],
            hiddencolumns = [
                tables.map(function(v, i) { return HiddenColumns['list']; })
            ];

        decode_example_csv_to_tables(example_data, base_category, 'Example').forEach(function(t) {
            tables.push(t);
            hiddencolumns.push(HiddenColumns['examples']);
        });

        decode_example_csv_to_tables(template_data, base_category, 'Template').forEach(function(t) {
            tables.push(t);
            hiddencolumns.push(HiddenColumns['templates']);
        });

        if (jS.kill) {
            // remove existing instance
            $sheet.getSheet().kill();
        }

        load_sheets_tables($sheet, tables, {
            editable: false,
            editableNames: false,
            allowToggleState: false,
            title: base_category,
            ignoreKeyDown: true,
            barMenus: false,
            autoFiller: false,
            resizableCells: false,
            resizableSheet: false,
            hiddenColumns: hiddencolumns,
            sheetAllOpened: function(ev, jS) {
                add_synonym_widget();
                add_copytable_button();
                drag_on();
                // remove spurious empty span if it's there
                $('.jSTabContainer span:empty').remove();
                $('<br>').insertBefore($('#sheetright .jSTabContainer span:contains(Template)').eq(0));
            }
        });
    };


    var update_right_sheet = function(base_category) {
        var prom_suggest = null,
            prom_examples = null,
            prom_templates = null;

        prom_suggest = $.ajax({
            url: '../CategorySheetQuestion_getListSuggestionTable',
            dataType: 'text',
            data: {
                base_category: base_category
            }
        }).fail(function(data, textStatus, errorThrown) {
            alert('Could not load suggestions: ' + errorThrown);
        });

        prom_examples = $.ajax({
            url: '../CategorySheetQuestion_getExampleTable',
            dataType: 'json',
            data: {
                base_category: base_category
            }
        }).fail(function(data, textStatus, errorThrown) {
            alert('Could not load examples: ' + errorThrown);
        });

        prom_templates = $.ajax({
            url: '../CategorySheetQuestion_getTemplateTable',
            dataType: 'json',
            data: {
                base_category: base_category
            }
        }).fail(function(data, textStatus, errorThrown) {
            alert('Could not load templates: ' + errorThrown);
        });


        $.when(prom_suggest, prom_examples, prom_templates)
            .done(function(resp_suggest, resp_examples, resp_templates) {
                var suggestion_data = resp_suggest[0],
                    example_data = resp_examples[0],
                    template_data = resp_templates[0];
                replace_right_sheet(base_category, suggestion_data, example_data, template_data);
            });
    };


    load_sheets_categories($('#sheetleft'),
                'CategorySheetQuestion_getCategoryDict',
                {
                    title: 'Categories',
                    autoFiller: false,
                    resizableSheet: false,
                    autoAddRows: true,
                    autoAddColumns: false,
                    sheetAddRow: update_category_title_changed,
                    sheetAddColumn: update_category_title_changed,
                    sheetRename: update_category_title_changed,
                    sheetCellEdited: update_category_title_changed,
                    sheetAdd: update_category_title_changed,
                    sheetDelete: update_category_title_changed,
                    sheetDeleteRow: update_category_title_changed,
                    sheetDeleteColumn: update_category_title_changed,
                    sheetSwitch: function(e, jS, i) {
                        var base_category = jS.obj.tables()[i].title;
                        update_right_sheet(base_category);
                    },
                    sheetAllOpened: function(ev, jS) {
                        // activate suggestions for the first base category
                        var starting_base_category = jS.obj.tables()[0].title;
                        update_right_sheet(starting_base_category);
                        add_updown_buttons();
                    }
                }
               );


    var add_synonym_widget = function(ev, jS) {
        var $select = $('<input id="suggestion-synonyms" type="hidden" value="Synonyms...">');

        $('#sheetright .jSTitle').parent().prepend($('<td style="width:50%">').append($select));

        $select.select2({
            placeholder: 'Synonyms...',
            data: [
                {id: 'Synonyms...', text: 'Synonyms...'}
            ]
        });

        $select.select2('enable', false);

    };


    var add_copytable_button = function(ev, jS) {
        var $copybutton = $('<button type="button" title="Overwrite configuration sheet with this table">Copy&nbsp;everything</button>');

        var copytable = function() {
            var $sheetleft = $('#sheetleft'),
                $sheetright = $('#sheetright'),
                srcjS = $sheetright.getSheet(),
                dstjS = $sheetleft.getSheet(),
                rowidx = 0,
                colidx = 0,
                cell_value = null;

            // remove everything but the header row
            for (rowidx=dstjS.rows().length; rowidx>0; rowidx-=1) {
                dstjS.deleteRow(rowidx);
            }

            // skip header row
            dstjS.controlFactory.addRowMulti(1, srcjS.rows().length-2, false);

            // skip header row
            for (rowidx=srcjS.rows().length; rowidx>0; rowidx-=1) {
                for (colidx=srcjS.cols().length+1; colidx>=1; colidx-=1) {
                    cell_value = $(sheetright).getCellValue(rowidx, colidx, srcjS.i);
                    $(sheetleft).setCellValue(cell_value, rowidx, colidx, dstjS.i);
                }
            }

            update_category_title_changed();
        };

        $('#sheetright .jSTitle').parent().prepend($('<td style="width:1%">').append($copybutton));

        $copybutton.on('click', function(ev) {
            $('<div></div>').appendTo('body')
                .html('<div><h6>Are you sure?</h6></div>')
                .dialog({
                    modal: true, title: 'Replace configuration table', zIndex: 10000, autoOpen: true,
                    width: 'auto', resizable: false,
                    buttons: {
                        Yes: function () {
                            $(this).dialog("close");
                            copytable();
                        },
                        No: function () {
                            $(this).dialog("close");
                        }
                    },
                    close: function (event, ui) {
                        $(this).remove();
                    }
                });
        });

    };

    $('.save').removeAttr('onclick');
    $('label:contains(Spreadsheet)').remove();

});
