/*jslint undef: true */
/*global $, document, window, alert, ZeroClipboard, setTimeout */

var drag_on;

jQuery(document).ready(function ($) {
    "use strict";

    var csv_parse = function(data) {
        // Using external CSV because the provided parser/tsv.js code is a little buggy
        return CSV.parse(data, {delim:',', quote:'"', rowdelim:'\n'});
    };


    drag_on = function() {

    var parent_sheet_el = function (el) {
        return $(el).closest('.jQuerySheet');
    };

    var all_synonym_rows = [];
// skip synonyms for now
//    $.get('../CategorySheetQuestion_getListSynonyms')
//        .done(function(data, textStatus, jqXHR) {
//            all_synonym_rows = csv_parse(data);
//        })
//        .fail(function(data, textStatus, errorThrown) {
//            alert('Could not load synonyms: ' + errorThrown);
//        });


    // skip title row with gt(0)
    $('#sheetright')
        .find('td.jSBarLeft')
        .parent()
        .draggable({
            helper: function(ev) {
                var $tr = $(ev.target).closest('tr'),
                    label = $tr.find('td').eq(6).text();
                var $ret = $('<div>', { 'class': 'ui-dragged' })
                            .text(label);
                return $ret;
            },
            containment: 'document',
            appendTo: 'body',
            zIndex: 10000,
            cursorAt: {left: -6, top: 10}
        })
        .mousedown(function(ev) {
            var $select = $('#suggestion-synonyms'),
                $sheet = $('#sheetright'),
                jS = $sheet.getSheet(),
                $tr = $(ev.target).closest('tr'),
                category_td = $tr.children()[5],
                title_td = $tr.children()[6],
                search_term = category_td.innerHTML,
                synonym_rows = [];

            jS.cellSetActive($(title_td));

            // Use this if handling 'click' event
            //if ($(this).is('.ui-draggable-dragging')) {
            //    return;
            //};

            // XXX TODO filter cached list of synonyms by base_category + term

            synonym_rows = all_synonym_rows.filter(function(row) {
                                // XXX TODO also filter by base_category == ???
                                return (row[2]===search_term);
                            });
            synonym_rows = synonym_rows.map(function(row, idx, array) {
                                return {id: row[3], text: row[3]};
                            });
            synonym_rows.unshift({id: '--', text: title_td.innerHTML});

            // placeholder only works with <select> elements.
            $select
                .select2('destroy')
                .html('<input id="suggestion-synonyms" type="hidden" />')
                .select2({data: synonym_rows})
                .off('change')              // disable event attached to a previous row
                .on('select2-open', function(ev) {
                    // if we are editing a cell, close it to avoid
                    // stealing keyboard events from the select box
                    // XXX does not save the value being edited
                    $('#sheetleft').getSheet().evt.cellEditAbandon();
                })
                .change(function(ev) {
                    // update cell value with the selected synonym
                    var cell = jS.getTdLocation(title_td);
                    $sheet.setCellValue(ev.val, cell.row, cell.col, jS.i);
                });
            $select.select2('val', '--');
            $select.select2('enable', true);
        });

    $('.jSUI').eq(0).droppable({
        accept: 'tr.ui-draggable',
        drop:function(ev, ui) {
            var src_parent = parent_sheet_el(ui.draggable),
                dst_parent = null,
                srcjS = null,
                dstjS = null;

            srcjS = $(src_parent).getSheet();
            var src_row = srcjS.getTdLocation(ui.draggable.children()[0]).row;

            // remove the helper to peek at what's behind
            $(ui.helper).remove();
            var td = document.elementFromPoint(ev.clientX, ev.clientY);

            if ($(td).hasClass('ui-resizable-handle')) {
                // handle widget (possibly on header row); we don't accept the drop
                return;
            }

            dst_parent = parent_sheet_el(td);

            dstjS = $(dst_parent).getSheet();

            var is_dst_row_empty = function(dst_row) {
                for (i=dstjS.cols().length-1; i>0; i-=1) {
                    cell_value = $(dst_parent).getCellValue(dst_row, i, dstjS.i);
                    if (cell_value.trim()) {
                        return false;
                    }
                }
                return true;
            }

            var dst_row = dstjS.getTdLocation(td).row;

            if (dst_row === 0) {
                // header row; we don't accept the drop
                return;
            } else if (isNaN(dst_row)) {
                // last row; add at the end
                dst_row = dstjS.rows().length;
                dstjS.controlFactory.addRow(dst_row-1, false);
            } else {
                // is row is empty, just replace the values, don't insert
                if (!is_dst_row_empty(dst_row)) {
                    // add before current row
                    dstjS.controlFactory.addRow(dst_row, true);
                }
            }

            var i = 0;
            var cell_value = null;
            for (i=srcjS.cols().length-1; i>0; i-=1) {
                cell_value = $(src_parent).getCellValue(src_row, i, srcjS.i);
                $(dst_parent).setCellValue(cell_value, dst_row, i, dstjS.i);
            }

        }
    });

    };

    // hover effect on rows is only available when 'drag-hover' class is active
    $(document).mousedown(function() { $('#sheetright').removeClass('drag-hover'); });
    $(document).mouseup(function() { $('#sheetright').addClass('drag-hover'); });
    $('#sheetright').addClass('drag-hover');

});
